import Vue from 'vue'
import VueRouter from 'vue-router'

// import Home from '../components/Home.vue'
const Home = () => import(/* webpackChunkName: "home" */ '../components/Home.vue')
// import Welcome from '../components/Welcome.vue'
const Welcome = () => import(/* webpackChunkName: "home" */ '../components/Welcome.vue')

// import Articles from '../components/article/Articles.vue'
const Articles = () => import(/* webpackChunkName: "Articles" */ '../components/article/Articles.vue')
const Add = () => import(/* webpackChunkName: "Articles" */ '../components/article/Add.vue')
const Edit = () => import(/* webpackChunkName: "Articles" */ '../components/article/Edit')
// import Add from '../components/article/Add.vue'
// import Edit from '../components/article/Edit'

// import Login from '../components/Login.vue'
const Login = () => import(/* webpackChunkName: "login" */ '../components/Login.vue')

Vue.use(VueRouter)

const routes = [{
  path: '/',
  redirect: '/login'
},
{
  path: '/login',
  name: 'login',
  component: Login
},
{
  path: '/home',
  name: 'home',
  component: Home,
  redirect: '/welcome',
  children: [{
    path: '/welcome',
    name: 'welcome',
    component: Welcome
  },
  {
    path: '/article',
    name: 'article',
    component: Articles
  },
  {
    path: '/article/add',
    name: 'add',
    component: Add
  },
  {
    path: '/article/edit/:id',
    name: 'edit',
    component: Edit
  }]
}
]

const router = new VueRouter({
  // mode: 'history', // 去除url中#
  routes
})

export default router
