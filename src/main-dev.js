import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
// 引入全局样式
import './assets/css/global.css'

// 富文本样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

// 导入进度条插件
import NProgress from 'nprogress'
// 导入进度条样式
import 'nprogress/nprogress.css'

import quillEditor from 'vue-quill-editor'

import axios from 'axios'

// 全局注册富文本组件
Vue.use(quillEditor)

// 请求在到达服务器之前，先会调用use中的这个回调函数来添加请求头信息
axios.interceptors.request.use(config => {
  // 当进入request拦截器，表示发送了请求，我们就开启进度条
  NProgress.start()
  // 必须返回config
  return config
})
// 在response拦截器中，隐藏进度条
axios.interceptors.response.use(config => {
  // 当进入response拦截器，表示请求已经结束，我们就结束进度条
  NProgress.done()
  return config
})

// 配置axio跟路径
axios.defaults.baseURL = 'http://47.107.156.169:3000/api/v1'
Vue.prototype.$http = axios

Vue.config.productionTip = false

// 时间过滤器
Vue.filter('dateFilter', function (val) {
  const time = new Date(val)
  const y = time.getFullYear()
  const m = (time.getMonth() + 1 + '').padStart(2, '0')
  const d = (time.getDate() + '').padStart(2, '0')

  const hh = (time.getHours() + '').padStart(2, '0')
  const mm = (time.getMinutes() + '').padStart(2, '0')
  const ss = (time.getSeconds() + '').padStart(2, '0')

  return y + '年' + m + '月' + d + '日' + '' + hh + ':' + mm + ':' + ss
})

// 路由导航守卫
router.beforeEach((to, from, next) => {
  // 如果用户访问login 就直接放行
  if (to.path === '/login') return next()
  // 从session中获取保存的 token值
  var token = sessionStorage.getItem('token')
  if (!token) return next('/login')
  next()
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
