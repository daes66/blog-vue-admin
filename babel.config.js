// 这是项目发布阶段 需要用到的 babel 插件
const prodPlugins = []
if (process.env.NODE_ENV === 'production') {
  prodPlugins.push('transform-remove-console')
}

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    [
      'component',
      {
        libraryName: 'element-ui',
        styleLibraryName: 'theme-chalk'
      }
    ],
    // 发布产品插件 展开运算符
    ...prodPlugins,
    // 配置路由懒加载插件
    '@babel/plugin-syntax-dynamic-import'
  ]
}
